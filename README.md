# Ansible Role: Kubectl

This role installs the [Kubectl](https://kubernetes.io/docs/tasks/tools/) binary on any supported host.

## Requirements

N/A

## Installing

The role can be installed by running the following command:

```bash
git clone https://gitlab.com/enmanuelmoreira/ansible-role-kubectl enmanuelmoreira.kubectl
```

Add the following line into your `ansible.cfg` file:

```bash
[defaults]
roles_path = ../
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    kubectl_version: 'v1.20.0'
    kubectl_platform: linux
    kubectl_arch: amd64

The path to the main Kubectl repo. Unlessy you need to override this for special reasons (e.g. running on servers without public Internet access), you should leave it as the default.

The location where the Helm binary will be installed.

    kubectl_bin_path: /usr/local/bin/kubectl

Specific vars for specific operative systems are into `vars` directory.  

Controls for the version of Helm to be installed. See [available Helm releases](https://github.com/helm/helm/releases/). You can upgrade or downgrade versions by changing the `helm_version` into `vars/Other.yml` file.

    helm_repo_path: "https://get.helm.sh"

## Tested on

* Ubuntu 20.04
* Ubuntu 18.04
* Rocky 8
* Fedora 35
* MacOS Monterrey

## Dependencies

None.

## Example Playbook

    - hosts: all
      roles:
        - role: enmanuelmoreira.kubectl

## License

MIT / BSD
